import argparse
import os
import zipfile
from glob import glob

from ev import start_logger, LineListingProcessor, LogProcessor, CaseProcessor, ListProcessor


def main(
        source: str,
        connection_string: str = "host=localhost dbname=ema user=import password=import",
        schema: str = "sep_2023"
):
    """ Main function for processing """
    index = 1
    zip_file = None
    if os.path.isfile(source):
        if zipfile.is_zipfile(source):
            zip_file = zipfile.ZipFile(source, 'r')
            files = list(filter(lambda n: n.endswith(".csv"), zip_file.namelist()))
        else:
            files = [source]  # single file, continue regularly
    else:
        files = sorted(glob(f'{source}**/*.csv', recursive=True))

    if len(files) == 0:
        print(f"{source} contains no files or is not a valid file")
    for file in files:
        print(f"Processing {file} ({index} of {len(files)})")
        index += 1
        try:
            if "log.csv" in file:
                _processor = LogProcessor(
                    file,
                    connection_string,
                    zip_file,
                    schema
                )
                _processor.run()
            elif "products.csv" in file:
                print(f"{file} is a products list")
                _processor = ListProcessor(
                    file,
                    connection_string,
                    zip_file,
                    schema
                )
                _processor.run()
            elif "substances.csv" in file:
                print(f"{file} is a substances list")
                _processor = ListProcessor(
                    file,
                    connection_string,
                    zip_file,
                    schema
                )
                _processor.run()
            elif "cases_per_country.csv" in file:
                _processor = CaseProcessor(
                    file,
                    connection_string,
                    zip_file,
                    schema
                )
                _processor.run()
            else:
                _processor = LineListingProcessor(
                    file,
                    connection_string,
                    zip_file,
                    schema
                )
                _processor.run()
        except ValueError as e:
            print(e)


if __name__ == '__main__':
    """ Default Python landing function """
    print("csv to pg started")
    _years = None
    _identifiers = None
    _schema = "public"
    parser = argparse.ArgumentParser(description='CSV to PG')
    parser.add_argument('source', type=str,
                        help='csv file, zip with csv files or directory with csv files')
    parser.add_argument('schema', type=str,
                        help='postgreSQL database schema to use for insertion')
    args = parser.parse_args()

    if not os.path.isfile(args.source) and not args.source.endswith('/'):
        args.source = args.source + '/'

    if args.schema is not None:
        _schema = args.schema

    logger = start_logger('.', 'EV Script')
    main(
        source=args.source,
        schema=_schema
    )
    print("csv to pg done")
