# Requirements

Make sure you have Python 3 and a PostgreSQL database

# Install dependencies

```python
python -m pip install -r requirements.txt
```

# Usage

Read all linelisting csv files from a directory into a PostgreSQL database
```python
python csv_to_pg.py ./downloads/2023-01-01
```

