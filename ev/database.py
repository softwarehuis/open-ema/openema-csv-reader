from datetime import datetime
from typing import List, Optional

import psycopg2


class DatabaseAdapter:
    """
    The general interface to the database.
    """

    def __insert(self, col_definition: List[tuple[str, str]], sub_type: Optional[str] = None):
        # Create the INSERT statement template
        if sub_type is not None:
            return f"INSERT INTO {self.schema}.{self.list_type}_{sub_type} ({', '.join([x[0] for x in col_definition])}) VALUES " + "{}"
        return f"INSERT INTO {self.schema}.{self.list_type} ({', '.join([x[0] for x in col_definition])}) VALUES " + "{}"

    def __columns(self, columns):
        """ Rewrite columns so PostgreSQL understands them """
        _columns = []
        for col in columns:
            _columns.append(f'"{col[0]}" {col[1]}')
        return _columns

    def __tuples(self, values):
        """ Create data tuples for bulk insertion into PostgreSQL """
        _tuples = []
        for line in values:
            formatted_values = []
            for value in line:
                if isinstance(value, datetime):
                    formatted_values.append(value.strftime('%Y-%m-%d'))
                else:
                    formatted_values.append(value)
            _tuples.append(tuple(formatted_values))
        return _tuples

    def __execute(self, query: str, tuples=None, label: str = None):
        """
        Run a command against PostgreSQL
        """
        conn = psycopg2.connect(self.connection_string)
        cursor = conn.cursor()
        try:
            if tuples is not None:
                cursor.execute(query, tuples)
            else:
                cursor.execute(query)
            conn.commit()
            if self.debug:
                print(cursor.query.decode())
            if cursor.rowcount > 0:
                if label is not None and cursor.rowcount > 1:
                    print(f"{cursor.rowcount} {label}s added")
                elif label is not None and cursor.rowcount == 1:
                    print(f"{cursor.rowcount} {label} added")
                else:
                    print(f"{cursor.rowcount} rows affected")
        except psycopg2.Error as e:
            conn.rollback()
            if self.debug:
                print(cursor.query.decode())
            print("Error message:", e)
            print(query)
            print(tuples)
            exit()
        finally:
            cursor.close()
            conn.close()

    def __init__(self, connection_string: str, list_type: str, schema: str, debug: bool = False):
        """
        Initiate the Database Adapter
        """
        self.schema = schema
        self.debug = debug
        self.connection_string = connection_string
        self.list_type = list_type
        self.line_listing_column_definition = [
            (f"{list_type}_id", "INTEGER NOT NULL"),
            ("local_number", "VARCHAR NOT NULL"),
            ("report_type", "VARCHAR"),
            ("report_date", "DATE"),
            ("qualification", "VARCHAR"),
            ("country", "VARCHAR"),
            ("age_group_lower_months", "SMALLINT"),
            ("age_group_upper_months", "SMALLINT"),
            ("age_group_reporter", "VARCHAR"),
            ("is_child_report", "BOOLEAN"),
            ("sex", "VARCHAR"),
            ("serious", "BOOLEAN")
        ]
        self.log_column_definition = [
            ("timestamp", "TIMESTAMP NOT NULL"),
            ("level", "VARCHAR NOT NULL"),
            ("lines", "INT"),
            ("message", "VARCHAR NOT NULL"),
            ("file", "VARCHAR"),
            ("url", "VARCHAR"),
            ("category", "VARCHAR NOT NULL"),
            ("identifier", "INTEGER"),
            ("serious", "BOOLEAN")
        ]
        self.list_column_definition = [
            (f"{list_type[:-1]}_id", "INTEGER NOT NULL"),
            ("name", "VARCHAR NOT NULL")
        ]
        self.case_column_definition = [
            ("type", "VARCHAR NOT NULL"),
            ("identifier", "INTEGER NOT NULL"),
            ("country", "VARCHAR NOT NULL"),
            ("value", "INT NOT NULL")
        ]
        self.reaction_column_definition = [
            (f"{list_type}_id", "INTEGER NOT NULL"),
            ("local_number", "VARCHAR NOT NULL"),
            ("report_date", "DATE"),
            ("reaction", "VARCHAR NOT NULL"),
            ("duration", "VARCHAR"),
            ("outcome", "VARCHAR"),
            ("seriousness_criteria", "VARCHAR"),
        ]

        self.drug_list_column_definition = [
            (f"{list_type}_id", "INTEGER NOT NULL"),
            ("local_number", "VARCHAR NOT NULL"),
            ("report_date", "DATE"),
            ("drugs", "VARCHAR"),
            ("characteristic", "VARCHAR"),
            ("indication", "VARCHAR"),
            ("action", "VARCHAR"),
            ("duration", "VARCHAR"),
            ("dose", "VARCHAR"),
            ("route", "VARCHAR")
        ]

        self.literature_column_definition = [
            (f"{list_type}_id", "INTEGER NOT NULL"),
            ("local_number", "VARCHAR NOT NULL"),
            ("report_date", "DATE"),
            ("literature", "VARCHAR")
        ]

    def create_line_listing_table(self):
        """ Generate SQL command to generate the line listing table if it does not exist """
        columns = self.__columns(self.line_listing_column_definition)
        _create_statement = f"CREATE TABLE IF NOT EXISTS {self.schema}.{self.list_type} ({', '.join(columns)})"
        self.__execute(_create_statement)

    def create_log_table(self):
        """ Generate SQL command to generate the line listing table if it does not exist """
        columns = self.__columns(self.log_column_definition)
        _create_statement = f"CREATE TABLE IF NOT EXISTS {self.schema}.{self.list_type} ({', '.join(columns)})"
        self.__execute(_create_statement)

    def create_list_table(self):
        """ Generate SQL command to generate the line listing table if it does not exist """
        columns = self.__columns(self.list_column_definition)
        _create_statement = f"CREATE TABLE IF NOT EXISTS {self.schema}.{self.list_type} ({', '.join(columns)})"
        self.__execute(_create_statement)

    def create_case_table(self):
        """ Generate SQL command to generate the line listing table if it does not exist """
        columns = self.__columns(self.case_column_definition)
        _create_statement = f"CREATE TABLE IF NOT EXISTS {self.schema}.case ({', '.join(columns)})"
        self.__execute(_create_statement)

    def create_reaction_table(self):
        """ Generate SQL command to generate the reaction table if it does not exist """
        columns = self.__columns(self.reaction_column_definition)
        _create_statement = f"CREATE TABLE IF NOT EXISTS {self.schema}.{self.list_type}_reaction ({', '.join(columns)})"
        self.__execute(_create_statement)

    def create_subject_drug_list_table(self):
        """ Generate SQL command to generate the subject drug list table if it does not exist """
        columns = self.__columns(self.drug_list_column_definition)
        _create_statement = f"CREATE TABLE IF NOT EXISTS {self.schema}.{self.list_type}_subject_drug_list ({', '.join(columns)})"
        self.__execute(_create_statement)

    def create_concomitant_drug_list_table(self):
        """ Generate SQL command to generate the concomitant drug list table if it does not exist """
        columns = self.__columns(self.drug_list_column_definition)
        _create_statement = f"CREATE TABLE IF NOT EXISTS {self.schema}.{self.list_type}_concomitant_drug_list ({', '.join(columns)})"
        self.__execute(_create_statement)

    def create_literature_table(self):
        """ Generate SQL command to generate the literature table if it does not exist """
        columns = self.__columns(self.literature_column_definition)
        _create_statement = f"CREATE TABLE IF NOT EXISTS {self.schema}.{self.list_type}_literature ({', '.join(columns)})"
        self.__execute(_create_statement)

    def bulk_insert_log(self, lines):
        """ Perform bulk insert for line listings """
        self.create_log_table()
        _tuples = self.__tuples(lines)
        if len(_tuples) > 0:
            records_list_template = ', '.join(['%s'] * len(_tuples))
            insert_query = self.__insert(self.log_column_definition).format(records_list_template)
            self.__execute(insert_query, _tuples, "log")

    def bulk_insert_list(self, lines):
        """ Perform bulk insert for line listings """
        self.create_list_table()
        _tuples = self.__tuples(lines)
        if len(_tuples) > 0:
            records_list_template = ', '.join(['%s'] * len(_tuples))
            insert_query = self.__insert(self.list_column_definition).format(records_list_template)
            self.__execute(insert_query, _tuples, "record")

    def bulk_insert_case(self, lines):
        """ Perform bulk insert for line listings """
        self.create_case_table()
        _tuples = self.__tuples(lines)
        if len(_tuples) > 0:
            records_list_template = ', '.join(['%s'] * len(_tuples))
            insert_query = self.__insert(self.case_column_definition).format(records_list_template)
            self.__execute(insert_query, _tuples, "case")

    def bulk_insert_lines(self, lines):
        """ Perform bulk insert for line listings """
        self.create_line_listing_table()
        _tuples = self.__tuples(lines)
        if len(_tuples) > 0:
            records_list_template = ', '.join(['%s'] * len(_tuples))
            insert_query = self.__insert(self.line_listing_column_definition).format(records_list_template)
            self.__execute(insert_query, _tuples, "line listing")

    def bulk_insert_reactions(self, reactions):
        """ Perform bulk insert for reactions """
        self.create_reaction_table()
        _tuples = self.__tuples(reactions)
        if len(_tuples) > 0:
            records_list_template = ', '.join(['%s'] * len(_tuples))
            insert_query = self.__insert(self.reaction_column_definition, "reaction").format(records_list_template)
            self.__execute(insert_query, _tuples, "reaction")

    def bulk_insert_subject_drug_list(self, subject_drug_list):
        """ Perform bulk insert for subject drug list """
        self.create_subject_drug_list_table()
        _tuples = self.__tuples(subject_drug_list)
        if len(_tuples) > 0:
            records_list_template = ', '.join(['%s'] * len(_tuples))
            insert_query = self.__insert(self.drug_list_column_definition, "subject_drug_list").format(records_list_template)
            self.__execute(insert_query, _tuples, "subject drug list")

    def bulk_insert_concomitant_drug_list(self, concomitant_drug_list):
        """ Perform bulk insert for concomitant drug list """
        self.create_concomitant_drug_list_table()
        _tuples = self.__tuples(concomitant_drug_list)
        if len(_tuples) > 0:
            records_list_template = ', '.join(['%s'] * len(_tuples))
            insert_query = self.__insert(self.drug_list_column_definition, "concomitant_drug_list").format(records_list_template)
            self.__execute(insert_query, _tuples, "concomitant drug list")

    def bulk_insert_literature(self, literature):
        """ Perform bulk insert for literature """
        self.create_literature_table()
        _tuples = self.__tuples(literature)
        if len(_tuples) > 0:
            records_list_template = ', '.join(['%s'] * len(_tuples))
            insert_query = self.__insert(self.literature_column_definition, "literature").format(records_list_template)
            self.__execute(insert_query, _tuples, "literature reference")
