""" Initialize with a file stem to find extra parameters and process """
import zipfile
from pathlib import Path
from typing import Union, Optional

from pyarrow import csv, Table

from ev import DatabaseAdapter


class ListProcessor:

    def __init__(
            self,
            file: Union[str, Path],
            connection_string: str,
            zip_file: Optional[zipfile.ZipFile] = None,
            schema: str = "import"):
        """ Initiate the LineListingProcessor """
        self.connection_string = connection_string
        self.table: Union[None, Table] = None

        self.zip_file = zip_file
        self.schema = schema
        if self.zip_file is None:
            self.file = Path(file)
            self.stem = file.stem
        else:
            self.file = file
            self.stem = file.split('/')[-1]
        self.lines = []
        print("stem", self.stem)
        if "substances.csv" in self.stem:
            self.stem = "substances"
            self.valid = True
        elif "products.csv" in self.stem:
            self.stem = "products"
            self.valid = True
        else:
            raise ValueError(f"{file} is not a valid cases per country file")

    def __iter_table(self):
        """ Iterate over the table batches, so we can stream data into PostgreSQL """
        for batch in self.table.to_batches():
            for row in zip(*batch.columns):
                yield row

    def __generate_table(self, delimiter: Optional[str] = ","):
        """ Construct a pyArrow table from the csv file """
        if self.zip_file is None:
            self.table = csv.read_csv(
                self.file,
                read_options=csv.ReadOptions(),
                parse_options=csv.ParseOptions(delimiter=delimiter)
            )
        elif isinstance(self.zip_file, zipfile.ZipFile):
            with self.zip_file.open(self.file, 'r') as zf:
                self.table = csv.read_csv(
                    zf,
                    parse_options=csv.ParseOptions(delimiter=delimiter)
                )

    def run(self):
        """ Run the LogLine processor """
        lines = []
        _database_adapter = DatabaseAdapter(self.connection_string, self.stem, self.schema)
        if self.table is None:
            self.__generate_table()

        for row in self.__iter_table():
            _line = ListLine(self.stem, row)
            lines.append(_line.get_line())
        _database_adapter.bulk_insert_list(lines)


class ListLine:
    """ Base class for the line listing with corresponding (relational) python objects constructed """
    def __init__(self, list_type: str, raw_line: any):
        self.raw_line = raw_line
        self.list_type = list_type
        self.row = [x.as_py() for x in raw_line]
        if len(self.row) == 2:  # country log
            self.identifier = self.row[0]
            self.name = self.row[1]

    def get_line(self):
        """ Get the regular line listing with all relational objects stripped
        So we have the information needed to create a 'flat' table
        """
        return (
            self.identifier,
            self.name
        )
