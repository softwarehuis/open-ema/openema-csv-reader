import logging

logger: logging.Logger


def start_logger(output_dir: str, name: str):
    log_formatter = logging.Formatter("%(asctime)s\t%(levelname)s\t%(message)s", "%Y-%m-%d %H:%M:%S")
    _logger = logging.getLogger(name)
    file_handler = logging.FileHandler("{0}/{1}.csv".format(output_dir, 'log'))
    print(f"log file: {output_dir}/log.csv")
    file_handler.setFormatter(log_formatter)
    logging.basicConfig(level=logging.INFO, handlers=[file_handler])
    return _logger
