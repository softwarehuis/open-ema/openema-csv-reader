""" Initialize with a file stem to find extra parameters and process """
import zipfile
from pathlib import Path
from typing import Union, Optional

from pyarrow import csv, Table

from ev import DatabaseAdapter


class LogProcessor:

    def __init__(
            self,
            file: Union[str, Path],
            connection_string: str,
            zip_file: Optional[zipfile.ZipFile] = None,
            schema: str = "import"):
        """ Initiate the LineListingProcessor """
        self.connection_string = connection_string
        self.table: Union[None, Table] = None

        self.zip_file = zip_file
        self.schema = schema
        if self.zip_file is None:
            self.file = Path(file)
            self.stem = file.stem
        else:
            self.file = file
            self.stem = file.split('/')[-1]
        self.lines = []
        print("stem", self.stem)
        if "log.csv" in self.stem:
            self.valid = True
        else:
            raise ValueError(f"{file} is not a valid log file")

    def __iter_table(self):
        """ Iterate over the table batches, so we can stream data into PostgreSQL """
        for batch in self.table.to_batches():
            for row in zip(*batch.columns):
                yield row

    def list_type(self):
        """ Determine the type log from the filename """
        if "log.csv" in self.stem:
            self.valid = True
            return "log"
        raise ValueError(f"{self.file} is not a log file")

    def __generate_table(self, delimiter: Optional[str] = "\t"):
        """ Construct a pyArrow table from the csv file """
        if self.zip_file is None:
            self.table = csv.read_csv(
                self.file,
                parse_options=csv.ParseOptions(delimiter=delimiter)
            )
        elif isinstance(self.zip_file, zipfile.ZipFile):
            with self.zip_file.open(self.file, 'r') as zf:
                self.table = csv.read_csv(
                    zf,
                    parse_options=csv.ParseOptions(delimiter=delimiter)
                )

    def run(self):
        """ Run the LogLine processor """
        lines = []
        _database_adapter = DatabaseAdapter(self.connection_string, self.list_type(), self.schema)
        if self.table is None:
            self.__generate_table()

        for row in self.__iter_table():
            _line = LogLine(self.list_type(), row)
            lines.append(_line.get_line())

        _database_adapter.bulk_insert_log(lines)


class LogLine:
    """ Base class for the line listing with corresponding (relational) python objects constructed """
    def __init__(self, list_type: str, raw_line: any):
        self.raw_line = raw_line
        self.list_type = list_type
        self.row = [x.as_py() for x in raw_line]
        if len(self.row) == 9:  # import log
            self.timestamp = self.row[0]
            self.level = self.row[1]
            self.lines = self.row[2]
            self.message = self.row[3]
            self.file = self.row[4]
            self.url = self.row[5]
            self.category = self.row[6]
            self.identifier = self.row[7]
            self.serious = self.seriousness(self.row[8])
        elif len(self.row) == 3:  # countries log
            self.timestamp = self.row[0]
            self.level = self.row[1]
            self.lines = None
            self.message = self.row[2]
            self.file = None
            self.url = None
            self.category = "country"
            self.identifier = None
            self.serious = None

    def seriousness(self, value: str):
        """ Determine the seriousness from the filename """
        if value is not None:
            if value == "not-serious":
                return False
            elif value == "serious":
                return True
        return None

    def get_line(self):
        """ Get the regular line listing with all relational objects stripped
        So we have the information needed to create a 'flat' table
        """
        return (
            self.timestamp,
            self.level,
            self.lines,
            self.message,
            self.file,
            self.url,
            self.category,
            self.identifier,
            self.serious
        )
