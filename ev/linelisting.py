""" Initialize with a file stem to find extra parameters and process """
import zipfile
from pathlib import Path
from typing import Union, Optional

from pyarrow import csv, Table

from ev.database import DatabaseAdapter


def get_last(value: tuple[any]):
    for i, var in enumerate(value):
        if i == len(value) - 1:
            return var


def flop(value: str):
    """
    There is an issue with brackets that are present in text and need to be flopped
    before processing to prevent errors
    """
    value = value.replace("(HIV-REMMER, ANTIRETROVIRALE MEDICATIE)", "|HIV-REMMER, ANTIRETROVIRALE MEDICATIE|")
    value = value.replace("(PH. EUR.)", "|PH. EUR.|")
    value = value.replace("(LIVE, ATTENUATED)", "|LIVE, ATTENUATED|")
    return value


def flip(value: str):
    """
    After flopping, we replace pipes (|) by brackets again to have the data in the original form available again.
    """
    value = value.replace("|HIV-REMMER, ANTIRETROVIRALE MEDICATIE|", "(HIV-REMMER, ANTIRETROVIRALE MEDICATIE)")
    value = value.replace("|PH. EUR.|", "(PH. EUR.)")
    value = value.replace("|LIVE, ATTENUATED|", "(LIVE, ATTENUATED)")
    return value


def get_value(value):
    """
    Normalize value and merge different representations for missing and None
    """
    return value if value not in ["Unknown", "n/a"] and len(value.strip()) > 0 else None


class LineListingProcessor:

    """ List of file names to ignore while processing """
    ignore_list = [
        "substances",
        "products",
        "organisations-all-versions",
        "locations-all-versions",
        "locations-all-versions.coords",
        "log"
    ]

    def __init__(
            self,
            file: Union[str, Path],
            connection_string: str,
            zip_file: Optional[zipfile.ZipFile] = None,
            schema: str = "import"):
        """ Initiate the LineListingProcessor """
        self.connection_string = connection_string
        self.table: Union[None, Table] = None

        self.zip_file = zip_file
        self.schema = schema
        if self.zip_file is None:
            self.file = Path(file)
            self.stem = file.stem
        else:
            self.file = file
            self.stem = file.split('/')[-1]
        self.lines = []

        if self.stem not in self.ignore_list:
            self.valid = True
        else:
            raise ValueError(f"{file} is not a valid line listing")

    def __iter_table(self):
        """ Iterate over the table batches, so we can stream data into PostgreSQL """
        for batch in self.table.to_batches():
            for row in zip(*batch.columns):
                yield row

    def seriousness(self):
        """ Determine the seriousness from the filename """
        _split = self.stem.split("_")
        if len(_split) > 2:
            if "not-serious" in _split:
                return False
            elif "serious" in _split:
                return True
        return None

    def list_type(self):
        """ Determine the type of line listing (either product or substance) from the filename """
        _type = self.stem.split("_")[0]
        if _type in ["product", "substance"]:
            self.valid = True
            return _type
        raise ValueError(f"{self.file} is not a product or substance line listing")

    def id(self):
        """ Get the product_id or substance_id from the filename """
        _type = self.stem.split("_")[1]
        return int(_type)

    def __generate_table(self, delimiter: Optional[str] = "\t"):
        """ Construct a pyArrow table from the csv file """
        if self.zip_file is None:
            self.table = csv.read_csv(
                self.file,
                parse_options=csv.ParseOptions(delimiter=delimiter)
            )
        elif isinstance(self.zip_file, zipfile.ZipFile):
            with self.zip_file.open(self.file, 'r') as zf:
                self.table = csv.read_csv(
                    zf,
                    parse_options=csv.ParseOptions(delimiter=delimiter)
                )

    def run(self):
        """ Run the line listing processor """
        lines = []
        reactions = []
        literature = []
        subject_drug_list = []
        concomitant_drug_list = []
        _database_adapter = DatabaseAdapter(self.connection_string, self.list_type(), self.schema)
        if self.table is None:
            self.__generate_table()

        for row in self.__iter_table():
            _line = LineListing(self.id(), self.list_type(), row, self.seriousness())
            lines.append(_line.get_line())
            _reactions = _line.get_reaction_list()
            if len(_reactions) > 0:
                reactions = reactions + _reactions

            _subject_drug_list = _line.get_subject_drug_list()
            if len(_subject_drug_list) > 0:
                subject_drug_list = subject_drug_list + _subject_drug_list
            _concomitant_drug_list = _line.get_concomitant_drug_list()
            if len(_concomitant_drug_list) > 0:
                concomitant_drug_list = concomitant_drug_list + _concomitant_drug_list
            _literature = _line.get_literature()
            if len(_literature) > 0:
                literature = literature + _literature

        _database_adapter.bulk_insert_lines(lines)
        _database_adapter.bulk_insert_reactions(reactions)
        _database_adapter.bulk_insert_literature(literature)
        _database_adapter.bulk_insert_subject_drug_list(subject_drug_list)
        _database_adapter.bulk_insert_concomitant_drug_list(concomitant_drug_list)


class LineListing:
    """ Base class for the line listing with corresponding (relational) python objects constructed """
    def __init__(self, identifier: int, list_type: str, raw_line: any, seriousness: Optional[bool] = None):
        self.serious = seriousness
        self.raw_line = raw_line
        self.identifier = identifier
        self.list_type = list_type
        self.row = [x.as_py() for x in raw_line]
        self.local_number = self.row[0]
        self.report_type = self.row[1]
        self.date = self.row[2]
        self.qualification = self.row[3]
        self.country = self.row[4]
        self.literature = LiteratureReference(self.row[5])
        self.age_group = AgeGroup(self.row[6])
        self.age_group_reporter = self.row[7] if self.row[7].lower() != "not specified" else None
        self.child_report: bool = True if self.row[8].lower() == "yes" else False
        self.sex = self.row[9] = self.row[9] if self.row[9].lower() != "not specified" else None
        self.reaction_list = ReactionList(self.row[10])
        self.subject_drug_list = DrugList(self.row[11])
        try:
            self.concomitant_drug_list = DrugList(self.row[12])
        except IndexError:
            print("Error", self.row[12])
            raise IndexError
        self.icsr_form = ICSRForm(self.row[13])

    def get_line(self):
        """ Get the regular line listing with all relational objects stripped
        So we have the information needed to create a 'flat' table
        """
        return (
            self.identifier,
            self.local_number,
            self.report_type,
            self.date,
            self.qualification,
            self.country,
            self.age_group.age_lower,
            self.age_group.age_upper,
            self.age_group_reporter,
            self.child_report,
            self.sex,
            self.serious
        )

    def get_reaction_list(self):
        """
        Explode the reactions into individual objects
        So we have the information needed to create a 'flat' table
        """
        _result = []
        for line in self.reaction_list.reactions:
            _result.append((self.identifier, self.local_number, self.date) + line)
        return _result

    def get_subject_drug_list(self):
        """
        Explode the subject drug list items into individual objects
        So we have the information needed to create a 'flat' table
        """
        _result = []
        for line in self.subject_drug_list.drugs:
            if line is not None:
                _result.append((self.identifier, self.local_number, self.date) + line)
        return _result

    def get_concomitant_drug_list(self):
        """
        Explode the concomitant drug list items into individual objects
        So we have the information needed to create a 'flat' table
        """
        _result = []
        for line in self.concomitant_drug_list.drugs:
            if line is not None:
                _result.append((self.identifier, self.local_number, self.date) + line)
        return _result

    def get_literature(self):
        """
        Explode the literature items into individual objects
        So we have the information needed to create a 'flat' table
        """
        _result = []
        for line in self.literature.literature:
            if line is not None:
                _result.append((self.identifier, self.local_number, self.date, line))
        return _result


class AgeGroup:
    """
    Determine the age group upper and lower values in months
    """
    def __init__(self, raw_value: any):
        self.age_lower = None
        self.age_upper = None
        self.raw_value = raw_value
        if raw_value == "0-1 Month":
            self.age_lower = 0
            self.age_upper = 1
        elif raw_value == "2 Months - 2 Years":
            self.age_lower = 2
            self.age_upper = 2 * 12
        else:
            _group = self.raw_value.replace(" Years", "").lower().split("-")
            if _group[0].strip() == "more than 85":
                self.age_lower = 85 * 12
            elif _group[0].strip() != "not specified":
                self.age_lower = int(_group[0]) * 12
                self.age_upper = int(_group[1]) * 12


class LiteratureReference:
    """ Class that checks and creates an individual literature object """
    def __init__(self, raw: any):
        self.literature = []
        _literature = raw.split(" - ")
        if _literature != ["Not available"]:
            self.literature = _literature


class DrugDetail(object):
    """ Class that checks and creates drug details """
    def __init__(
            self,
            raw: Optional[str] = None
    ):
        self.duration = None
        self.dose = None
        self.route = None
        if raw is not None:
            _list = raw.split(" - ")
            self.duration = get_value(_list[0])
            if len(_list) > 1:
                self.dose = get_value(_list[1])
            if len(_list) > 2:
                self.route = get_value(_list[2])

    def as_tuple(self):
        return self.duration, self.dose, self.route


class Drug(object):
    """ Class for drugs """
    def __init__(
            self,
            raw: str
    ):
        self.drug = None
        self.characteristic = None
        self.indication = None
        self.action = None
        self.detail = None
        if raw.lower() not in ["not reported", "..."]:
            # Nasty replace because some moron decided to use brackets
            raw = flop(raw)
            _split = raw.split(" (")
            if len(_split) == 1:
                raise RuntimeError(f"could not split {raw}")
            self.drug = flip(_split[0])
            _list = _split[1].replace("]", "").split("[")
            _own_raw = _list[0]
            _own_items = _own_raw.split(" - ")
            self.characteristic = get_value(_own_items[0])

            if len(_own_items) > 1:
                self.indication = get_value(_own_items[1])
            if len(_own_items) > 2:
                self.action = get_value(_own_items[2])

            if len(_list) == 2:
                _detail_raw = _list[1]
            else:
                _detail_raw = None

            self.detail = DrugDetail(_detail_raw)

    def as_tuple(self):
        if self.drug is None:
            return None

        return (
            self.drug,
            self.characteristic,
            self.indication,
            self.action,
        ) + self.detail.as_tuple()


class DrugList(object):
    """ Class for drug lists """
    def __init__(self, raw: str):
        self.drugs = []
        if raw != "Not reported":
            _split = raw.split("<BR><BR>")
            for _entry in _split:
                _entry = _entry.rstrip(',').rstrip(')')
                _drug = Drug(_entry)
                self.drugs.append(_drug.as_tuple())


class Reaction:
    """ Class for reactions """
    def __init__(
            self,
            raw: str
    ):
        self.reaction = None
        self.duration = None
        self.outcome = None
        self.seriousness_criteria = None

        _split = raw.split(" (")
        self.reaction = _split[0]
        _list = _split[1].split(" - ")

        self.duration = get_value(_list[0])
        if len(_list) > 1:
            self.outcome = get_value(_list[1])
        if len(_list) > 2:
            self.seriousness_criteria = get_value(_list[2])

    def as_tuple(self):
        return self.reaction, self.duration, self.outcome, self.seriousness_criteria


class ReactionList:
    """ Class for reaction lists"""
    def __init__(self, raw: any):
        self.reactions = []
        _split = raw.split("<BR><BR>")
        for _entry in _split:
            _reaction = Reaction(_entry.rstrip(',').rstrip(')'))
            self.reactions.append(_reaction.as_tuple())


class ICSRForm:
    """ Class for ICSRForm (not used in import into PostgreSQL)"""
    def __init__(self, raw_value: any):
        self.raw_value = raw_value
